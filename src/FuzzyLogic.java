

public class FuzzyLogic {
	
	
	public FuzzyLogic() 
	{
		
	}
	public double[][] blindssharpen(double lumens)
	{
		double blinds_low = 0, blinds_medium = 0, blinds_high = 0;
		double bright_vdark = 0, bright_dark = 0, bright_medium = 0, bright_bright = 0, bright_vbright = 0;
		double fuzzy_vbright = 0, fuzzy_bright = 0, fuzzy_medium = 0;
		double[][] blinds, brightness, fuzzyblinds;
		brightness = new double[6][2501];
		blinds = new double[4][2501];
		fuzzyblinds = new double[5][2501];
		
		for(int i = 0; i <= 2500; i++)
		{
			double x = i * 0.04;
			double y = (i * 0.02) + 50;
			brightness[0][i] = x;
			blinds[0][i] = y;
			fuzzyblinds[0][i] = y;
			//BRIGHTNESS TERM
			//VDARK TERM
			if((x >= 0) & (x <= 20))
			{
				bright_vdark = (20 - x)/(20 - 0);
			}
			else
			{
				bright_vdark = 0;
			}
			//DARK TERM
			if((x >= 10) & (x <= 25))
			{
				bright_dark=(x-10)/(25-10);
			}
			else if((x >= 25) & (x <= 40))
			{
				bright_dark=(40-x)/(40-25);
			}
			else
			{
				bright_dark = 0;
			}
			//MEDIUM TERM
			if((x >= 30) & (x <= 50))
			{
				bright_medium=(x-30)/(50-30);
			}
			else if((x >= 50) & (x <= 70))
			{
				bright_medium=(70-x)/(70-50);
			}
			else
			{
				bright_medium = 0;
			}
			//BRIGHT TERM
			if((x >= 60) & (x <= 75))
			{
				bright_bright=(x-60)/(75-60);
			}
			else if((x >= 75) & (x <= 90))
			{
				bright_bright=(90-x)/(90-75);
			}
			else
			{
				bright_bright = 0;
			}
			//VBRIGHT TERM
			if((x >= 80) & (x <= 100))
			{
				bright_vbright=(x-80)/(100-80);
			}
			else
			{
				bright_vbright = 0;
			}
			//PRZYPISANIE TERMOW
			brightness[1][i] = bright_vdark;
			brightness[2][i] = bright_dark;
			brightness[3][i] = bright_medium;
			brightness[4][i] = bright_bright;
			brightness[5][i] = bright_vbright;
			
		//BLINDS TERMS
			//LOW TERM
			if((y >= 50) & (y <= 75))
			{
				 blinds_low=(75 - y)/(75-50);
			}
			else
			{
				blinds_low = 0;
			}
			//MEDIUM TERM
			if((y >= 62.5) & (y <= 75))
			{
				blinds_medium=(y-75)/(75-62.5);
			}
			else if((y >= 75) & (y <= 87.5))
			{
				blinds_medium=(87.5-y)/(87.5-75);
			}
			else
			{
				blinds_medium = 0;
			}
			//HIGH TERM
			if((y >= 75) & (y <= 100))
			{
				blinds_high=(y-75)/(100-75);
			}
			else
			{
				blinds_high = 0;
			}
			//PRZYPISANIE TERMOW BLINDS
			blinds[1][i] = blinds_low;
			blinds[2][i] = blinds_medium;
			blinds[3][i] = blinds_high;
		//ROZMYWANIE
			
			if(x == lumens/25)
			{
				if(bright_medium > 0)
				{
					fuzzy_medium = bright_medium;
					System.out.println("MEDIUM " + fuzzy_medium);
				}
				if(bright_bright > 0)
				{
					fuzzy_bright = bright_bright;
					System.out.println("BRIGHT " + fuzzy_bright);
				}
				if(bright_vbright > 0)
				{
					fuzzy_vbright = bright_vbright;
					System.out.println("VBRIGHT " + fuzzy_vbright);
				}
			}
			
			else
			{
				
			}
			
		//WYOSTRZANIE
		//REGULY:
			//JESLI MEDIUM TO LOW
			//JESLI BRIGHT TO MEDIUM
			//JESLI VBRIGHT TO HIGH
			fuzzyblinds[1][i] = Math.min(fuzzy_medium, blinds_low);
			fuzzyblinds[2][i] = Math.min(fuzzy_bright, blinds_medium);
			fuzzyblinds[3][i] = Math.min(fuzzy_vbright, blinds_high);
			fuzzyblinds[4][i] = Math.max(Math.max(fuzzyblinds[1][i], fuzzyblinds[2][i]), fuzzyblinds[3][i]);
			System.out.println(fuzzy_medium + " " + blinds_low);
			
		}
		return fuzzyblinds;

	}
	public double[][] lightssharpen(double lumens)
	{
		double lights_high = 0, lights_medium = 0, lights_low = 0;
		double bright_vdark = 0, bright_dark = 0, bright_medium = 0, bright_bright = 0, bright_vbright = 0;
		double fuzzy_vdark = 0, fuzzy_dark = 0, fuzzy_medium = 0;
		double[][] lights, brightness, fuzzylights;
		lights = new double[4][2501];
		brightness = new double[6][2501];
		fuzzylights = new double[5][2501];
		for(int i = 0; i <= 2500; i= i+1)
		{
			double x = i * 0.04;
			double y = i * 0.04;
			brightness[0][i] = x;
			lights[0][i] = y;
			fuzzylights[0][i] = y;
			//BRIGHTNESS TERM
			//VDARK TERM
			if((x >= 0) & (x <= 20))
			{
				bright_vdark = (20 - x)/(20 - 0);
			}
			else
			{
				bright_vdark = 0;
			}
			//DARK TERM
			if((x >= 10) & (x <= 25))
			{
				bright_dark=(x-10)/(25-10);
			}
			else if((x >= 25) & (x <= 40))
			{
				bright_dark=(40-x)/(40-25);
			}
			else
			{
				bright_dark = 0;
			}
			//MEDIUM TERM
			if((x >= 30) & (x <= 50))
			{
				bright_medium=(x-30)/(50-30);
			}
			else if((x >= 50) & (x <= 70))
			{
				bright_medium=(70-x)/(70-50);
			}
			else
			{
				bright_medium = 0;
			}
			//BRIGHT TERM
			if((x >= 60) & (x <= 75))
			{
				bright_bright=(x-60)/(75-60);
			}
			else if((x >= 75) & (x <= 90))
			{
				bright_bright=(90-x)/(90-75);
			}
			else
			{
				bright_bright = 0;
			}
			//VBRIGHT TERM
			if((x >= 80) & (x <= 100))
			{
				bright_vbright=(x-80)/(100-80);
			}
			else
			{
				bright_vbright = 0;
			}
			//PRZYPISANIE TERMOW
			brightness[1][i] = bright_vdark;
			brightness[2][i] = bright_dark;
			brightness[3][i] = bright_medium;
			brightness[4][i] = bright_bright;
			brightness[5][i] = bright_vbright;
			
		//LIGHTS TERM			
			//HIGH TERM
			if((y >= 0) & (y <= 50))
			{
				lights_high=(50-y)/(50-0);
			}
			else
			{
				lights_high = 0;
			}
			//MEDIUM TERM
			if((y >= 25) & (y <= 50))
			{
				lights_medium=(y-25)/(50-25);
			}
			else if((y >= 50) & (y <= 75))
			{
				lights_medium=(75-y)/(75-50);
			}
			else
			{
				lights_medium = 0;
			}
			//LOW TERM
			if((y >= 50) & (y <= 100))
			{
				lights_low=(y-50)/(100-50);
			}
			else
			{
				lights_low = 0;
			}
			//PRZYPISANIE TERMOW
			lights[1][i] = lights_high;
			lights[2][i] = lights_medium;
			lights[3][i] = lights_low;
			
			//ROZMYWANIE	
			if(x == lumens/25)
			{
				
				if(bright_vdark > 0)
				{
					fuzzy_vdark = bright_vdark;
					System.out.println("VDARK " + fuzzy_vdark);
					
				}
				if(bright_dark > 0)
				{
					fuzzy_dark = bright_dark;
					System.out.println("DARK " +fuzzy_dark);
				}
				if(bright_medium > 0)
				{
					fuzzy_medium = bright_medium;
					System.out.println("MEDIUM " +fuzzy_medium);
				}	
				
			}
		//WYOSTRZANIE
		//REGULY:
			//JESLI VDARK TO HIGH
			//JESLI DARK TO MEDIUM
			//JESLI MEDIUM TO LOW
			fuzzylights[1][i] = Math.min(fuzzy_vdark, lights_high);
			fuzzylights[2][i] = Math.min(fuzzy_dark, lights_medium);
			fuzzylights[3][i] = Math.min(fuzzy_medium, lights_low);
			fuzzylights[4][i] = Math.max(Math.max(fuzzylights[1][i], fuzzylights[2][i]), fuzzylights[3][i]);
			
			//System.out.println(fuzzylights[4][i]);
			
		}
		return fuzzylights;
		
	}
	public double Sharpen(double [][] terms)
	{
		double sharp = 0, counter = 0, divider = 0;
		for (int i = 0; i <= 2500; i++)
		{
			counter = counter + terms[4][i] * i;
			divider = divider + terms[4][i];
		}
		sharp = counter / divider;
		return sharp;
	}
public static void main(String[] args) {
	FuzzyLogic dupa = new FuzzyLogic();
	dupa.blindssharpen(1250);
	//dupa.lightssharpen(1250);
	//System.out.println(dupa.Sharpen(dupa.blindssharpen(1250)));
	//System.out.println(dupa.Sharpen(dupa.lightssharpen(1250)));
}
}


